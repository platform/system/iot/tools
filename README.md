emmc_image.py is a tool to generate an eMMC USER image.

Requirement:

*   Python 2.7
*   simg2img
    *   Used to unsparse Android sparse images
    *   Source code available at https://android.googlesource.com/platform/system/core/+/master/libsparse/
    *   Prebuilt binaries
        *   Debian >= Stretch or Ubuntu >= 17.10
            *   sudo apt-get install simg2img
    *   The `simg2img` binary should either be available in `PATH` or `SIMG2IMG_BIN` should be set to its path.
*   This currently lives in AOSP here: https://android.googlesource.com/platform/system/iot/tools/


**Factory image downloaded from console in both slots:**

```
./emmc_image.py --out emmc.zip --factory_files console_build.zip --all-slots
```


**Custom images in slot A, factory image downloaded from console in slot B:**

```
./emmc_image.py --out emmc.zip --factory_files console_build.zip \
--all-slots \
--partition boot_a custom_boot.img \
--partition system_a custom_system.img \
--partition vbmeta_a custom_vbmeta.img
```


**Dump partition table:**

```
parted emmc.img
```
